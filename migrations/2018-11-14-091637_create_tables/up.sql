create table films(
        id serial primary key,
        popular int not null,
        year int not null,
        mature boolean default false not null,
        silent boolean default false not null,
        mellow boolean default false not null,
        surreal boolean default false not null,
        docu boolean default false not null,
        legit boolean default true not null,
        limitdate date default null,
        img int unique not null,
        title text not null,
        mins int not null
);

create table auteurs(
        id serial primary key,
        auteur text not null,
        filmid int not null,
        foreign key (filmid) references films(id) on delete cascade
);

create table intros(
        id serial primary key,
        intro text not null,
        filmid int not null,
        foreign key (filmid) references films(id) on delete cascade
);

create table links(
        id serial primary key,
        src int not null,
        vid text not null,
        filmid int not null,
        foreign key (filmid) references films(id) on delete cascade
);