use chrono::prelude::{Utc};
use models::*;
use std::vec::Vec;
use diesel::prelude::*;
use diesel::pg::PgConnection;
use diesel::dsl::any;
use diesel::r2d2::{PooledConnection, ConnectionManager, Pool};
use actix::prelude::*;
use actix_web::*;
use failure::Error;

pub type Connection = PooledConnection<ConnectionManager<PgConnection>>;
pub struct DbExecutor(pub Pool<ConnectionManager<PgConnection>>);

impl Actor for DbExecutor {
    type Context = SyncContext<Self>;
}

pub struct LSF {
    pub magic_num: u8,
    pub input_str: Option<String>,
}

impl Message for LSF {
    type Result = Result<Film, Error>;
}

impl Handler<LSF> for DbExecutor {
    type Result = Result<Film, Error>;
    fn handle(&mut self, msg: LSF, _: &mut Self::Context) -> Self::Result {
        let conn: Connection = self.0.get().unwrap();
        list_selected_films(conn, msg)
    }
}

fn list_selected_films(conn: Connection, lsf: LSF) -> Result<Film, Error> {

    if lsf.magic_num >= 60 {
        println!("No categories checked");
        // TODO: return;
    };

    use schema::films::dsl::*;
    let mut query = films.into_boxed();
    let arr = int8_to_bin_arr(lsf.magic_num);

    if lsf.magic_num < 4 {
        println!("All categories checked");
    } else {

        let mut categories = Vec::new();
        get_categories(arr.to_vec(), &mut categories);
        println!("{:?}", categories);

        match categories[0] {
            1 => query = query.filter(silent),
            2 => query = query.filter(mellow),
            3 => query = query.filter(surreal),
            4 => query = query.filter(docu),
            _ => println!("impossible"),
        }
        categories.remove(0);
        // println!("{:?}", categories);
        if categories.len() > 0 {
            for c in categories {
                match c {
                    1 => query = query.or_filter(silent),
                    2 => query = query.or_filter(mellow),
                    3 => query = query.or_filter(surreal),
                    4 => query = query.or_filter(docu),
                    _ => println!("unknown category"),
                }
            }
        }
    }

    match &lsf.input_str {
        None => {
            println!("Nothing typed in");
        },
        Some(word) => {
            let foundfilmids = search_str(&conn, &word);
            if  foundfilmids.is_empty() {
                println!("No search results");
                // TODO: return;
            } else {
                println!("{:?}", foundfilmids);
                query = query.filter(id.eq(any(foundfilmids)));
            }
        },
    }

    match arr[4] {
        0 => query = query.order(id.desc()),
        1 => query = query.order((popular.desc(), id.desc())),
        2 => query = query.order((year.desc(), popular.desc())),
        3 => query = query.order((year.asc(), popular.desc())),
        _ => println!("unknown sorting"),
    }

    let mut results = query
        .limit(22)
        .load::<Film>(&conn)
        .expect("Error loading films");

    // println!("Displaying {} films", results.len());

    for film in &results {
        let now = Utc::today().naive_utc();
        let days_left = if let Some(x) = film.limitdate {
          Some(x.signed_duration_since(now).num_days() + 365)
        }
        else {
          None
        };

        let ppl = get_auteurs(&conn, &film);
        let intro = get_intro(&conn, &film);

        let mut kwds = Vec::new();
        get_kwds([film.mature, film.silent, film.mellow, film.surreal, film.docu].to_vec(), &mut kwds);
        println!("{} {} {} {} {:?} {} {:?} {} {:?}", film.id, film.popular, film.year, film.title, ppl, intro, kwds, film.limitdate.is_some(), days_left);
    }
    Ok(results.pop().unwrap())
}

fn int8_to_bin_arr(x: u8) -> [u8;5] {
    let b0 : u8 = ((x >> 5) & 0x1) as u8;
    let b1 : u8 = ((x >> 4) & 0x1) as u8;
    let b2 : u8 = ((x >> 3) & 0x1) as u8;
    let b3 : u8 = ((x >> 2) & 0x1) as u8;
    let b4 : u8 = ((x >> 0) & 0x3) as u8;
    // println!("{} {} {} {} {}", b0, b1, b2, b3, b4);
    return [b0, b1, b2, b3, b4]
}

fn search_str(conn: &Connection, pattern: &String) -> Vec<i32> {
    use schema::films::dsl::*;
    return films
        .filter(id.eq(any(select_pattern_intro(conn, pattern))).or(id.eq(any(select_pattern_auteur(conn, pattern)))))
        .or_filter(title.ilike(format!("%{}%", pattern)))
        .select(id)
        .order(id.asc())
        .load::<i32>(conn)
        .expect("Error: search_str");
}

fn select_pattern_intro(conn: &Connection, pattern: &String) -> Vec<i32> {
    use schema::intros::dsl::*;
    return intros
        .filter(intro.ilike(format!("%{}%", pattern)))
        .select(filmid)
        .load::<i32>(conn)
        .expect("Error: select_pattern_intro");
}

fn select_pattern_auteur(conn: &Connection, pattern: &String) -> Vec<i32> {
    use schema::auteurs::dsl::*;
    return auteurs
        .filter(auteur.ilike(format!("%{}%", pattern)))
        .select(filmid)
        .load::<i32>(conn)
        .expect("Error: select_pattern_auteur");
}

fn get_auteurs(conn: &Connection, film: &Film) -> Vec<String> {
    use schema::auteurs::dsl::*;
    return Auteur::belonging_to(film)
        .select(auteur)
        .load::<String>(conn)
        .expect("Error loading auteurs");
}

fn get_intro(conn: &Connection, film: &Film) -> String {
    use schema::intros::dsl::*;
    return Intro::belonging_to(film)
        .select(intro)
        .first::<String>(conn)
        .expect("Error loading intro");
}

fn get_categories(v: Vec<u8>, r: &mut Vec<u8>) {
    if v[0] == 0 { r.push(1); } 
    if v[1] == 0 { r.push(2); } 
    if v[2] == 0 { r.push(3); } 
    if v[3] == 0 { r.push(4); }
}

fn get_kwds(v: Vec<bool>, r: &mut Vec<String>) {
    if v[0] { r.push("mature".to_string()) }
    if v[1] { r.push("silent".to_string()) }
    if v[2] { r.push("mellow".to_string()) }
    if v[3] { r.push("surrealist".to_string()) }
    if v[4] { r.push("documentary".to_string()) }
}
