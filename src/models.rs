use schema::*;
use chrono::prelude::{NaiveDate};

#[derive(Identifiable, Queryable)]
#[table_name="films"]
pub struct Film {
    pub id: i32,
    pub popular: i32,
    pub year: i32,
    pub mature: bool,
    pub silent: bool,
    pub mellow: bool,
    pub surreal: bool,
    pub docu: bool,
    pub legit: bool,
    pub limitdate: Option<NaiveDate>,
    pub img: i32,
    pub title: String,
    pub mins: i32,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Film, foreign_key = "filmid")]
#[table_name="auteurs"]
pub struct Auteur {
    pub id: i32,
    pub auteur: String,
    pub filmid: i32,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Film, foreign_key = "filmid")]
#[table_name="intros"]
pub struct Intro {
    pub id: i32,
    pub intro: String,
    pub filmid: i32,
}

#[derive(Identifiable, Queryable, Associations, PartialEq, Debug)]
#[belongs_to(Film, foreign_key = "filmid")]
#[table_name="links"]
pub struct Link {
    pub id: i32,
    pub src: i32,
    pub vid: String,
    pub filmid: i32,
}
