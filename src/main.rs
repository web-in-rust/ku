extern crate chrono;
#[macro_use]
extern crate diesel;
extern crate r2d2;
extern crate dotenv;
extern crate actix;
extern crate actix_web;
extern crate env_logger;
extern crate num_cpus;
extern crate futures;
extern crate failure;

use diesel::prelude::*;
use diesel::r2d2::ConnectionManager;
use dotenv::dotenv;
use std::env;
use actix::prelude::*;
use actix_web::{
    fs,
    pred,
    server,
    middleware,
    App,
    AsyncResponder,
    FutureResponse,
    HttpRequest,
    HttpResponse,
    Error,
    Result
};
use actix_web::http::{
    Method,
    StatusCode
};
use actix_web::middleware::session::{
    self,
    RequestSession
};
use futures::future::*;

mod models;
mod schema;
mod db;

use db::{DbExecutor, LSF};

struct AppState {
    db: Addr<DbExecutor>,
}

fn main() {
    env::set_var("RUST_LOG", "actix_web=debug");
    env::set_var("RUST_BACKTRACE", "1");
    env_logger::init();
    let sys = actix::System::new("ku");

    dotenv().ok();
    let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
    let manager = ConnectionManager::<PgConnection>::new(database_url);
    let pool = r2d2::Pool::builder()
        .build(manager)
        .expect("Failed to create pool");;
    let addr = SyncArbiter::start(num_cpus::get(), move || DbExecutor(pool.clone()));

    server::new(move || {
        App::with_state(AppState{db: addr.clone()})
            .middleware(middleware::Logger::default())
            .middleware(session::SessionStorage::new(
                    session::CookieSessionBackend::signed(&[0; 32]).secure(false)
            ))
            .handler("/assets", fs::StaticFiles::new("static").unwrap())
            .resource("/", |r| r.method(Method::GET).a(|req| {
                println!("{:?}", req);
                index(req)
             }))
            .resource("/ajaxcall", |r| r.method(Method::POST).a(|req| {
                println!("{:?}", req);

                let mut temp: LSF = LSF {
                    magic_num: 0,
                    input_str: None,
                };
                // Do something with session here
                if let Some(num) = req.session().get::<u8>("magic_num").expect("wtf") {
                    temp.magic_num = num;
                } else {
                    let _ = req.session().set("input_str", &temp.magic_num);
                }
                if let Some(Some(word)) = req.session().get::<Option<String>>("input_str").expect("wtf") {
                    temp.input_str = Some(word);
                } else {
                    let _ = req.session().set("input_str", &temp.input_str);
                }

                let word = " ito ".trim();
                answer(req, LSF {
                    magic_num: 44,
                    input_str: if word.len() < 1 { None } else { Some(word.to_string())}
                })
            }))
            .default_resource(|r| {
                r.method(Method::GET).f(p404);
                r.route().filter(pred::Not(pred::Get())).f(|_req| HttpResponse::MethodNotAllowed());
            })
    })
    .bind("0.0.0.0:8080")
    .expect("can't bind to 0.0.0.0:8080")
    .shutdown_timeout(0) // default 60 sec
    .start();

    println!("server on at 0.0.0.0:8080");
    let _ = sys.run();
}

fn index(_req: &HttpRequest<AppState>) -> FutureResult<HttpResponse, Error> {
    result(
        Ok(HttpResponse::build(StatusCode::OK)
           .content_type("text/html; charset=utf-8")
           .body(include_str!("../static/index.html")
        )))
}

fn answer(req: &HttpRequest<AppState>, lsf: LSF) -> FutureResponse<HttpResponse> {
    req.state()
        .db
        .send(lsf)
        .from_err()
        .and_then(|_| {
            Ok(HttpResponse::build(StatusCode::OK)
                .content_type("text/html; charset=utf-8")
                .body(include_str!("../static/index.html"))
            )
        })
        .responder()
}

fn p404(_req: &HttpRequest<AppState>) -> Result<fs::NamedFile> {
    Ok(
        fs::NamedFile::open("static/404.html")?.set_status_code(StatusCode::NOT_FOUND))
}
