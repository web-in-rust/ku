table! {
    auteurs (id) {
        id -> Int4,
        auteur -> Text,
        filmid -> Int4,
    }
}

table! {
    films (id) {
        id -> Int4,
        popular -> Int4,
        year -> Int4,
        mature -> Bool,
        silent -> Bool,
        mellow -> Bool,
        surreal -> Bool,
        docu -> Bool,
        legit -> Bool,
        limitdate -> Nullable<Date>,
        img -> Int4,
        title -> Text,
        mins -> Int4,
    }
}

table! {
    intros (id) {
        id -> Int4,
        intro -> Text,
        filmid -> Int4,
    }
}

table! {
    links (id) {
        id -> Int4,
        src -> Int4,
        vid -> Text,
        filmid -> Int4,
    }
}

joinable!(auteurs -> films (filmid));
joinable!(intros -> films (filmid));
joinable!(links -> films (filmid));

allow_tables_to_appear_in_same_query!(
    auteurs,
    films,
    intros,
    links,
);
