#!/usr/bin/env lua

if #arg < 2 then
  print('USAGE: insert [data.csv] [dbname]')
  os.exit()
end

local driver = require "luasql.postgres"
env = assert (driver.postgres())
con = assert (env:connect(arg[2]))

file = io.open(arg[1], "r")
if not file then return nil end

io.input(file)
io.read()

function string:split (sep)
  local sep, fields = sep or ":", {}
  local pattern = string.format("([^%s]+)", sep)
  self:gsub(pattern, function(c) fields[#fields+1] = c end)
  return fields
end

function isempty(s)
  return s == nil or s == ''
end

function inf2nil(s)
  if s == "infinity" then return nil
  else return s
  end
end

for line in file:lines() do
  t = line:split(",")
  cur = assert (con:execute(string.format([[
    select id, img from films where img='%d']], t[1])
  ))
  row = cur:fetch ({}, "a")
  if isempty(row) then

    if inf2nil(t[8]) == nil then
      res_film = assert (con:execute(string.format([[
        insert into films values (default, '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', null, '%d', '%s', '%d')]],  
        t[1], t[2], t[6], t[9], t[10], t[11], t[12], t[7], t[3], t[4], t[5])
      ))
    else
      res_film = assert (con:execute(string.format([[
        insert into films values (default, '%d', '%d', '%s', '%s', '%s', '%s', '%s', '%s', '%s', '%d', '%s', '%d')]],  
        t[1], t[2], t[6], t[9], t[10], t[11], t[12], t[7], t[8], t[3], t[4], t[5])
      ))
    end
    print('inserted Film:', t[4])

    -- auteurs
    if not isempty(t[15]) then
      auteurs = t[15]:split("|")
      for k,v in next,auteurs,nil do
        res_auteur = assert (con:execute(string.format([[
          insert into auteurs values 
          (default, '%s', (select id from films where img = '%d' order by id desc limit 1))]], v, t[3])
        ))
        print('inserted auteur:', v)
      end
    end

    -- intros
    res_intros = assert (con:execute(string.format([[
      insert into intros values  (default, '%s', (select id from films where img = '%d' order by id desc limit 1))]],  t[16], t[3])
    ))

    -- links
    res_links = assert (con:execute(string.format([[
      insert into links values  (default, '%d', '%s', (select id from films where img = '%d' order by id desc limit 1))]],  t[13], t[14], t[3])
    ))

  else
    print('ALREADY:', row.id, row.img)
  end

  cur:close()
end

io.close(file)

con:close()
env:close()
